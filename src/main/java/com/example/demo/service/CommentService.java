package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Comment;
import com.example.demo.repository.CommentRepository;

@Service
public class CommentService {
	@Autowired
	CommentRepository commentRepository;

	public List<Comment> findAllComment() {
		return commentRepository.findAll();
	}

//	public List<Comment> findComment(Integer id) {
//		List<Comment> comment = (List<Comment>) commentRepository.findById(id).orElse(null);
//		return comment;
//	}

	public void saveComment(Comment comment) {
		commentRepository.save(comment);
	}

	public Comment editComment(Integer id) {
		Comment comment = (Comment) commentRepository.findById(id).orElse(null);
		return comment;
	}

	public void deleteComment(Integer id) {
		commentRepository.deleteById(id);
	}

}
