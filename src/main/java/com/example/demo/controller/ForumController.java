package com.example.demo.controller;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Comment;
import com.example.demo.entity.Report;
import com.example.demo.service.CommentService;
import com.example.demo.service.ReportService;

@Controller
public class ForumController {
	@Autowired
	ReportService reportService;
	@Autowired
	CommentService commentService;

	@GetMapping
	public ModelAndView top(@RequestParam(value ="startDate", required = false) String startDate, @RequestParam(value ="endDate", required = false) String endDate) {
		ModelAndView mav = new ModelAndView();
		List<Report> contentData = null;
		Timestamp since =Timestamp.valueOf("2020-01-01 00:00:00");
		Timestamp until =new Timestamp(System.currentTimeMillis());

		if(!ObjectUtils.isEmpty(startDate)) {
			since = Timestamp.valueOf(startDate + "00:00:00");
		}

		if(!ObjectUtils.isEmpty(endDate)) {
			until = Timestamp.valueOf(endDate + "23:59:59");
		}

		if(!ObjectUtils.isEmpty(startDate) || !ObjectUtils.isEmpty(endDate)) {
			contentData = reportService.findAllReport(until, until);
		} else {
			contentData = reportService.findAllReport();
		}

		mav.setViewName("/top");

		mav.addObject("contents", contentData);
		return mav;
	}

	@GetMapping("/new")
	public ModelAndView newContent() {
		ModelAndView mav = new ModelAndView();

		Report report = new Report();

		mav.setViewName("/new");

		mav.addObject("formModel", report);
		return mav;
	}

	@PostMapping("/add")
	public ModelAndView addContent(@ModelAttribute("formModel") Report report) {
		reportService.saveReport(report);

		return new ModelAndView("redirect:/");
	}

	@DeleteMapping("/delete/{id}")
	public ModelAndView deleteContent(@PathVariable Integer id) {
		reportService.deleteReport(id);

		return new ModelAndView("redirect:/");
	}

	// 編集画面を表示させる
	@GetMapping("/edit/{id}")
	public ModelAndView editContent(@PathVariable Integer id) {
	ModelAndView mav = new ModelAndView();

	Report report = reportService.editReport(id);

	mav.addObject("formModel", report);
	mav.setViewName("/edit");

	return mav;
	}

	// 編集処理
	@PutMapping("/update/{id}")
	public ModelAndView updateContent (@PathVariable Integer id, @ModelAttribute("formModel") Report report) {

	report.setId(id);

	reportService.saveReport(report);

	return new ModelAndView("redirect:/");
	}

	// Report詳細画面を表示させる
	@GetMapping("/show/{id}")
	public ModelAndView showContent(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();
		// ReportCONTENTの取得
		Report report = reportService.showReport(id);

		// commentsCOMMENTの取得
		List<Comment> commentData = commentService.findAllComment();

		mav.addObject("formModel", report);

		mav.addObject("comments", commentData);
		mav.setViewName("/show");

		return mav;
	}

	@PostMapping("/comment/add")
	public ModelAndView addContent(@ModelAttribute("formModel") Comment comment) {

		commentService.saveComment(comment);

//		return new ModelAndView("redirect:/show/{id}");
		return new ModelAndView("redirect:/");
	}
//	COMMENT削除処理
	@DeleteMapping("/commentDelete/{id}")
	public ModelAndView deleteomment(@PathVariable Integer id) {
		commentService.deleteComment(id);

		return new ModelAndView("redirect:/");
	}


	// COMMENT編集画面を表示させる
	@GetMapping("/commentEdit/{id}")
	public ModelAndView editComment(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();

		Comment comment = commentService.editComment(id);

		mav.addObject("formModel", comment);
		mav.setViewName("/commentEdit");

		return mav;
	}

	// COMMENT編集処理
		@PutMapping("/comment/update/{id}")
		public ModelAndView updateComment (@PathVariable Integer id, @ModelAttribute("formModel") Comment comment) {

		comment.setId(id);

		commentService.saveComment(comment);

		return new ModelAndView("redirect:/");
		}
}
